import pandas as pd
from grr import GRR
from print_colors import OKGREEN, ENDC
from math_utils import rmse, frequencies

# Import the data.
data = pd.read_csv('./FIMU_2017_Vhs.csv')
ages = data["Age"]
number_of_data = len(ages)

# Print the different possibilities of ages.
V = ages.unique()
print(f'All classes of the attribute "Age": {V}')

# My list of epsilons.
epsilons = [0.1, 1, 10]

# My real values. A dictionnary which store for every real value (Vi) their number of appearances.
real_values = ages.value_counts().to_dict()

# My randomized values. A dictionnary which store for every randomized value value their number of appearances.
randomized_values = {}

# Create the generator who generates the randomized responses.
grr = GRR()

# For each epsilon in [0.1, 1, 10].
for epsilon in epsilons:
    print(f'{OKGREEN}\nRun with epsilon = {epsilon}{ENDC}')

    # Update the parameters of my generator.
    grr.set_parameters(epsilon, V)
    
    print(f'p: {grr.p}')
    print(f'q: {grr.q}')

    # We initialize all the possibilities to 0 appearance.
    for Vi in real_values:
        randomized_values[Vi] = 0
    
    # For every Vi of the real values, we compute its randomized value and store it.
    for Vi, count in real_values.items():
        for i in range(count):
            output = grr.randomized_response(Vi)
            randomized_values[output] += 1  

    # Print the amount of real values in each class.
    print("\nReal values:")
    print(real_values)

    # Print the amount of randomized values in each class.
    print("\nRandomized values:")
    print(randomized_values)

    # Print the amount of estimated values resulting from the unbiased estimator of the GRR.
    estimated_values = grr.estimates(randomized_values, number_of_data)
    print(f'\nEstimated values:')
    print(estimated_values)

    # Print the frequency of real values in each class.
    frequency_real_values = frequencies(real_values, number_of_data)
    print(f'\nFrequency real values:')
    print(frequency_real_values)

    # Print the frequency of estimated values in each class.
    frequency_estimated_values = frequencies(estimated_values, number_of_data)
    print(f'\nFrequency estimated values:')
    print(frequency_estimated_values)

    # Print the RMSE between the distribution of real values and estimated ones.
    rmse_val = rmse(real_values, estimated_values, number_of_data)
    print(f'\nRMSE: {rmse_val}')