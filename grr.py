from math import exp
import numpy as np
from random import randint, random

class GRR:
    def set_parameters(self, epsilon, V):
        self.j = len(V)
        self.p = exp(epsilon) / (exp(epsilon) + self.j - 1)
        self.q = (1 - self.p) / (self.j - 1)
        self.V = V

    def randomized_response(self, Vi):        
        """
            Randomizes a Vi value.
        """

        proba = random()

        if proba < self.p:
            return Vi
        else:            
            proba = randint(0, self.j - 2)            
            V_without_Vi = np.delete(self.V, np.where(self.V == Vi))
            return V_without_Vi[proba]

    def estimates(self, randomized_values, n):
        """
            Estimates the values according to the randomized values and the probabilities of the GRR.
        """

        unbiased_values = {}

        for key, Ni in randomized_values.items():
            unbiased_values[key] = round((Ni - n * self.q) / (self.p - self.q))

        return unbiased_values

