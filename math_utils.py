from math import sqrt, trunc

def rmse(real_values, estimated_values, number_of_data):
    sum_rmse = 0

    for key in real_values:
        sum_rmse += (real_values[key] - estimated_values[key])**2
 
    return round(sqrt(sum_rmse / number_of_data), 2)

def frequencies(dict, number_of_data):
    frequency_value = {}

    for key, count in dict.items():
        percentage = round(count / number_of_data * 100, 2)
        frequency_value[key] = str(percentage) + '%'

    return frequency_value

def truncate(number, decimals=0):
    """
    Returns a value truncated to a specific number of decimal places.
    """

    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer.")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more.")
    elif decimals == 0:
        return trunc(number)

    factor = 10.0 ** decimals
    return trunc(number * factor) / factor